    <?php

// Include Stylesheet
function load_stylesheets()
{
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), false, 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('style', get_template_directory_uri() . '/style.css', array(), false, 'all');
    wp_enqueue_style('style');
}

add_action('wp_enqueue_scripts', 'load_stylesheets');

// Include Jquery
function include_jquery()
{
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-min.js', '', 1, true);

    add_action('wp_enqueue_scripts', 'jquery');
}

add_action('wp_enqueue_scripts', 'include_jquery');
function loadjs()
{
    wp_register_script('bootstrap',get_template_directory_uri().'/js/bootstrap.min.js','',1,true);
    wp_enqueue_script('bootstrap');
    
    wp_register_script('customjs', get_template_directory_uri() . '/js/scripts.js', '', 1, true);
    wp_enqueue_script('customjs');
}

add_action('wp_enqueue_scripts', 'loadjs');

// Add menu Setting in Dashboard
add_theme_support('menus');

add_theme_support('post-thumbnails');

require get_stylesheet_directory() . '/inc/theminimallist-customizer.php';
$variable=new TheMinimalist_Customizer();

// CART

/**
 * Add Cart icon and count to header if WC is active
 */
function my_wc_cart_count() {

    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

        $count = WC()->cart->cart_contents_count;
        ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php
        if ( $count > 0 ) {
            ?>
            <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
            <?php
        }
        ?></a><?php
    }

}

/**
 * Ensure cart contents update when products are added to the cart via AJAX
 */
function my_header_add_to_cart_fragment( $fragments ) {

    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php
    if ( $count > 0 ) {
        ?>
        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
        <?php
    }
    ?></a><?php

    $fragments['a.cart-contents'] = ob_get_clean();

    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment' );


add_action( 'your_theme_header_top', 'my_wc_cart_count' );

//menu location


register_nav_menus(

    array(

        'top-menu' => __('Top Menu', 'theme'),
        'footer-menu' => __('Footer Menu', 'theme'),

    )
);


add_image_size('smallest', 300, 300, true);
add_image_size('largest', 800, 800, true);

//changes in user dashboard

/**
  * Edit my account menu order
  */

 function my_account_menu_order() {
 	$menuOrder = array(
 	
 	);
 	return $menuOrder;
 }
 add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );

//footer widget

function mytheme_widgets_init()
{
    register_sidebar(array(
        'name' => __( 'Footer Widget Area', 'mytheme' ),
        'id' => 'footer-sidebar',
        'description' => __( 'Appears on the footer, which has its own widgets', 'mytheme' ),
        'before_widget' => '
<div id="%1$s" class="widgetfooter">',
        'after_widget' => '</div>
',
        'before_title' => '
<h3 class="widget-title">',
        'after_title' => '</h3>
',
    ) );
}
add_action( 'widgets_init', 'mytheme_widgets_init' );

//Theme override
function customtheme_add_woocommerce_support()
{
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'customtheme_add_woocommerce_support' );

/* REMOVE ADD TO CART BUTTON ON PRODUCT ARCHIVE (SHOP) */

function remove_loop_button(){
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
}
add_action('init','remove_loop_button');

/* ADD NEW BUTTON THAT LINKS TO PRODUCT PAGE FOR EACH PRODUCT */

add_action('woocommerce_after_shop_loop_item','replace_add_to_cart');

function replace_add_to_cart() {
    global $product;
    
    
    $link = $product->get_permalink();
    $buy_now_link = $product->get_attribute('amazon_link');
    $buy_now_link = "Coming Soon";
    echo "<div class='text-center'><a class='product-link' href='".$link."'>View</a> <span class='product-link'> | Coming soon</span>";

    //  $buy_now_link = "<a class='product-link' href='".esc_url($buy_now_link)."'>".__('Buy Now', 'textdomain')."</a>";
  //  echo "<div class='text-center'><a class='product-link' href='".$link."'>View</a>". ((get_post_meta($product->id, '_upcoming', true ) ? "<span class='product-link'> | Coming soon</span>" : "<span class='product-link'> | </span>".$buy_now_link."</div>"));

}
add_action('template_redirect', 'shop_page_redirect');
function shop_page_redirect()
{ 
	 if (is_shop())
	 {
		 // don't remove the quotes(')
		  wp_redirect(home_url().'/products');
		  exit;
	 }
}

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 15);


add_filter('wc_product_sku_enabled', 'remove_product_page_sku');
 
function remove_product_page_sku($enabled) {
    if ( !is_admin() && is_product() ) {
        return false;
    }
 
    return $enabled;
}

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

add_filter( 'woocommerce_get_availability_text', 'custom_stock_text', 99, 2 );
  
function custom_stock_text( $availability, $product ) {
  $softgels = $product->get_attribute('softgels');
  return ($softgels > 0 ) ? ("Quantity : ".$softgels." softgel") : "";
}


// function woocommerce_quantity_input( $args = array(), $product = null, $echo = true ) {
  
//   if ( is_null( $product ) ) {
//       $product = $GLOBALS['product'];
//   }
 
//   $defaults = array(
//       'input_id' => uniqid( 'quantity_' ),
//       'input_name' => 'quantity',
//       'input_value' => '1',
//       'classes' => apply_filters( 'woocommerce_quantity_input_classes', array( 'input-text', 'qty', 'text' ), $product ),
//       'max_value' => apply_filters( 'woocommerce_quantity_input_max', -1, $product ),
//       'min_value' => apply_filters( 'woocommerce_quantity_input_min', 0, $product ),
//       'step' => apply_filters( 'woocommerce_quantity_input_step', 1, $product ),
//       'pattern' => apply_filters( 'woocommerce_quantity_input_pattern', has_filter( 'woocommerce_stock_amount', 'intval' ) ? '[0-9]*' : '' ),
//       'inputmode' => apply_filters( 'woocommerce_quantity_input_inputmode', has_filter( 'woocommerce_stock_amount', 'intval' ) ? 'numeric' : '' ),
//       'product_name' => $product ? $product->get_title() : '',
//   );
 
//   $args = apply_filters( 'woocommerce_quantity_input_args', wp_parse_args( $args, $defaults ), $product );
  
//   // Apply sanity to min/max args - min cannot be lower than 0.
//   $args['min_value'] = max( $args['min_value'], 0 );
//   // Note: change 20 to whatever you like
//   $args['max_value'] = 0 < $args['max_value'] ? $args['max_value'] : 20;
 
//   // Max cannot be lower than min if defined.
//   if ( '' !== $args['max_value'] && $args['max_value'] < $args['min_value'] ) {
//       $args['max_value'] = $args['min_value'];
//   }
  
//   $options = '';
    
//   for ( $count = $args['min_value']; $count <= $args['max_value']; $count = $count + $args['step'] ) {
 
//       // Cart item quantity defined?
//       if ( '' !== $args['input_value'] && $args['input_value'] >= 1 && $count == $args['input_value'] ) {
//         $selected = 'selected';      
//       } else $selected = '';
 
//       $options .= '<option value="' . $count . '"' . $selected . '>' . $count . '</option>';
 
//   }
     
//   $string = '<div class="quantity"><select class="quantity-select" name="' . $args['input_name'] . '">' . $options . '</select></div>';
 
//   if ( $echo ) {
//       echo $string;
//   } else {
//       return $string;
//   }
  
// }

/**
 * Remove related products output on single product page
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

//Remove WooCommerce Tabs
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );


function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['description'] );      	// Remove the description tab
    $tabs['reviews']['title'] = __( 'Ratings & Reviews' );
    return $tabs;

}

// add_action( 'woocommerce_after_single_product', 'add_bottom_banner',10 );

// function add_bottom_banner(){
//     $page = get_posts( array( 'name' => 'reuse','post_type' => 'page' ));
//     if ( $page )
//     {
//         echo $page[0]->post_content;
//     }
// }

add_filter('woocommerce_checkout_fields', 'addBootstrapToCheckoutFields' );
function addBootstrapToCheckoutFields($fields) {
    foreach ($fields as &$fieldset) {
        foreach ($fieldset as &$field) {
            $field['class'][] = 'form-group'; 
            $field['input_class'][] = 'form-control p-2';
        }
    }
    return $fields;
}



add_action( 'after_setup_theme', 'product_image_zoom' );
 
function product_image_zoom() {
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}

remove_action( 'woocommerce_review_before', 'woocommerce_review_display_gravatar');

add_filter( 'woocommerce_get_breadcrumb', function($crumbs, $Breadcrumb){
        $shop_page_id = wc_get_page_id('shop'); 
        if($shop_page_id > 0 && !is_shop()) { 
            $new_breadcrumb = [
                _x( 'Shop', 'breadcrumb', 'woocommerce' ), //Title
                get_permalink(wc_get_page_id('shop')) // URL
            ];
            $crumbs[1] = $new_breadcrumb;
        }   
        return $crumbs;
    }, 10, 2 );
    
 
add_filter( 'default_checkout_billing_state', 'change_default_checkout_state' );
  
function change_default_checkout_state() {
  return 'BC'; // state code
}


function calculate_shipping_cost( $cost, $method ) {
   $cart_item_count = WC()->cart->get_cart_contents_count();
   $shipping_weight = round((260 * $cart_item_count) / 454);
   
   $country = WC()->customer->get_shipping_country();
   if($country == "CA"){
       if($shipping_weight > 1){
           $cost = ($shipping_weight * 3.8) + 3;
       }else if($shipping_weight = 1){
           $cost = 9.50;
       }
   } else if($country == "HK" || $country == "CN"){
       if($shipping_weight > 2){
           $cost = ($shipping_weight * 3.8) + 1.5;
       }else if($shipping_weight <= 2){
           $cost = 9.00;
       }
   }
   return $cost;
}
add_filter( 'woocommerce_shipping_rate_cost', 'calculate_shipping_cost', 10, 2 );


add_action('wp_footer', 'billing_country_update_checkout', 50);
function billing_country_update_checkout() {
    if ( ! is_checkout() ) return;
    ?>
    <script type="text/javascript">
    jQuery(function($){
        $('select#billing_country, select#shipping_country').on( 'change', function (){
            var t = { updateTimer: !1,  dirtyInput: !1,
                reset_update_checkout_timer: function() {
                    clearTimeout(t.updateTimer)
                },
                trigger_update_checkout: function() {
                    t.reset_update_checkout_timer(), t.dirtyInput = !1,
                    $(document.body).trigger("update_checkout")
                }
            };
            $(document.body).trigger('update_checkout');
        });
    });
    </script>
    <?php
}

/**** Custom Product Tabs - Remove Tab Title ****/
add_filter( 'yikes_woocommerce_custom_repeatable_product_tabs_heading', '__return_false' );

/**** Copyright Sentence ****/
function create_copyright() {
$all_posts = get_posts( 'post_status=publish&order=ASC' );
$first_post = $all_posts[0];
$first_date = $first_post->post_date_gmt;
_e( 'Copyright &copy; ' );
if ( substr( $first_date, 0, 4 ) == date( 'Y' ) ) {
echo date( 'Y' );
} else {
echo substr( $first_date, 0, 4 ) . date( 'Y' );
}
echo '<span style="text-transform: capitalize;"><strong> ' . get_bloginfo( 'name' ) . '</strong></span> . ';
_e( 'All rights reserved.' );
}

/**** Remove Flat Rate word ****/
/**
 * @snippet       Removes shipping method labels @ WooCommerce Cart / Checkout
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=484
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.5.6
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
add_filter( 'woocommerce_cart_shipping_method_full_label', 'bbloomer_remove_shipping_label', 10, 2 );
  
function bbloomer_remove_shipping_label( $label, $method ) {
$new_label = preg_replace( '/^.+:/', '', $label );
return $new_label;
}

add_action( 'wp_footer', 'update_cart_on_qty_change' ); 
 
function update_cart_on_qty_change() { 
   if (is_cart()) { 
      ?> 
     <script>
        $('div.woocommerce').on('change', '.qty', function(){
            $("[name='update_cart']").prop("disabled", false);
            $("[name='update_cart']").trigger("click"); 
        });
    </script>
      <?php 
   } 
}

// Remove product in the cart using ajax
function warp_ajax_product_remove()
{
    // Get mini cart
    ob_start();

    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item)
    {
        if($cart_item['product_id'] == $_POST['product_id'] && $cart_item_key == $_POST['cart_item_key'] )
        {
            WC()->cart->remove_cart_item($cart_item_key);
        }
    }

    WC()->cart->calculate_totals();
    WC()->cart->maybe_set_cart_cookies();

    woocommerce_mini_cart();

    $mini_cart = ob_get_clean();

    // Fragments and mini cart are returned
    $data = array(
        'fragments' => apply_filters( 'woocommerce_add_to_cart_fragments', array(
                'div.widget_shopping_cart_content' => '<div class="widget_shopping_cart_content">' . $mini_cart . '</div>'
            )
        ),
        'cart_hash' => apply_filters( 'woocommerce_add_to_cart_hash', WC()->cart->get_cart_for_session() ? md5( json_encode( WC()->cart->get_cart_for_session() ) ) : '', WC()->cart->get_cart_for_session() )
    );

    wp_send_json( $data );

    die();
}

add_action( 'wp_ajax_product_remove', 'warp_ajax_product_remove' );
add_action('wp_ajax_nopriv_product_remove', 'warp_ajax_product_remove');

add_filter( 'woocommerce_ship_to_different_address_checked', '__return_false' );

add_filter( 'woocommerce_quantity_input_args', 'bloomer_woocommerce_quantity_changes', 10, 2 );
   
function bloomer_woocommerce_quantity_changes( $args, $product ) {
   if ( is_cart() ) {
      $args['min_value'] = 1; 
   }
   return $args;
}

// add_action('wp_enqueue_scripts', 'live_validation' );
// add_action('wp_ajax_validate_email', 'validate_email_input');
// add_action('wp_ajax_nopriv_validate_email', 'validate_email_input');

// function live_validation() {
//     wp_enqueue_script( "validate_email", get_stylesheet_directory_uri() . '/check-email.js', array( 'jquery' ) );
//     wp_localize_script( "validate_email", "validateEmail", array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
// }
// function validate_email_input() {
//     global $wpdb;
//     $email = $_POST['billing_email'];
//     if ( email_exists($email) ) {
//         echo "exist"; 
//     } else {
//         echo "not_exist";
//     }
// }

add_action('woocommerce_after_checkout_validation', 'email_custom_validation_for_login');

function email_custom_validation_for_login( $posted ) {
    global $wpdb;
    $email = $_POST['billing_email'];
    if ( email_exists($email)) {
         wc_add_notice( __( "An account is already registered with your email address. Please log in.", 'woocommerce' ), 'error' );
    }
}
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

// Add custom link in product single next to add to cart button
add_action( 'woocommerce_single_product_summary', 'change_add_to_cart_section', 30 );
function change_add_to_cart_section() {
    global $product;
    if($product->get_stock_quantity()>0){
        $softgels = $product->get_attribute('softgels');
        $softgels = ($softgels > 0 ) ? ("Quantity : ".$softgels." softgel") : "";
        
        $buy_now_link = $product->get_attribute('amazon_link');
       // $buy_now_link = "<a class='single_add_to_cart button alt rounded-0' href='".esc_url($buy_now_link)."'>".__('Buy Now', 'textdomain')."</a>";
        $buy_now_link = "<a class='single_add_to_cart button alt rounded-0' href='".esc_url($buy_now_link)."'>".__('Coming Soon', 'textdomain')."</a>";

        $name = get_template_directory_uri()."/images/amazon.png";
        $img = '<img src="'.$name.'" alt="Amazon" width="60"/>';
        $string = "Products can be purchased through ".$img."<br/>or Authorized Dealers";
        echo "<div class='mb-4'>".$softgels."</div><div><div class='float-left'>".$string."</div><div class='float-right'>".$buy_now_link."</div><div class='clearfix'></div></div>";
    }
}
//Remove quantity field
// function woocommerce_quantity_input( $args = array(), $product = null, $echo = true ) {
//     $name = get_template_directory_uri()."/images/amazon.png";
//     $img = '<img src="'.$name.'" alt="Amazon" width="60"/>';
//     $string = "<div class='d-inline-block'>Products can be purchased through ".$img."<br/>or Authorized Dealers</div>";
//      if ( $echo ) {
//       echo $string;
//   } else {
//       return $string;
//   }
// }
?>



