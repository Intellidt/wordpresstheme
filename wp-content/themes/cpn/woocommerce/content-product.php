<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
    return;
}
?>
<li <?php wc_product_class( '', $product ); ?>>
    <?php
    /**
     * Hook: woocommerce_before_shop_loop_item.
     *
     * @hooked woocommerce_template_loop_product_link_open - 10
     */
    do_action( 'woocommerce_before_shop_loop_item' );

    /**
     * Hook: woocommerce_before_shop_loop_item_title.
     *
     * @hooked woocommerce_show_product_loop_sale_flash - 10
     * @hooked woocommerce_template_loop_product_thumbnail - 10
     */
    ?>
    <div class="d-block position-relative">
        <?php
            do_action( 'woocommerce_before_shop_loop_item_title' );
        ?>
    	<div class="overlay">
    		<div class="p-3">
    			<div class="T1 mt-2 font-weight-bold text-center">WHO</div>
    			<div class="T1 text-center mt-3"><?php echo $product->get_attribute('product_hover_description') ?></div>    			
    		</div>
			<div class="text-center mt-4 overlaybutton">
    				<button type="button" class="btn rounded-0 T1">VIEW DETAIL</button>
			</div>
    	</div>
	</div>
    <div class="product-icon" style="background-image:url(<?php echo bloginfo('template_url').'/images/'.$product->get_attribute('product_icon'); ?>); display: none;">
    </div>
    <?php
    /**
     * Hook: woocommerce_shop_loop_item_title.
     *
     * @hooked woocommerce_template_loop_product_title - 10
     */
    do_action( 'woocommerce_shop_loop_item_title' );

    /**
     * Hook: woocommerce_after_shop_loop_item_title.
     *
     * @hooked woocommerce_template_loop_rating - 5
     * @hooked woocommerce_template_loop_price - 10
     */
    do_action( 'woocommerce_after_shop_loop_item_title' );

    /**
     * Hook: woocommerce_after_shop_loop_item.
     *
     * @hooked woocommerce_template_loop_product_link_close - 5
     * @hooked woocommerce_template_loop_add_to_cart - 10
     */
	?>
	<div class="mt-5">	
	<?php
    do_action( 'woocommerce_after_shop_loop_item' );
    ?>			
	</div>
</li>
