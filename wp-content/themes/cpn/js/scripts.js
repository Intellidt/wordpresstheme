function toggle_write_review(){
    $("#review_form_wrapper").slideToggle("slow","linear");
}
// jQuery(document).ready(function($) {
//      $('input[name=billing_email]').on('blur', function() {
//         var input = $('input[name=billing_email]');
//         var input_value = $(input).val();
//         $("#login_email_error").addClass("d-none");
//         $(input).removeClass("error");
//         $(input).parents("#billing_email_field").addClass("woocommerce-validated").removeClass("woocommerce-invalid woocommerce-invalid-required-field");
//         $.post( validateEmail.ajaxurl, { action:'validate_email', billing_email:input_value }, function(data) {
//             data = $.trim(data);
//             if(data == "exist0"){
//                 $("#login_email_error").removeClass("d-none");
//                 $(input).addClass("error");
//                 $(input).parents("#billing_email_field").removeClass("woocommerce-validated").addClass("woocommerce-invalid woocommerce-invalid-required-field");
//             }
//         });
//     });
// });
// Ajax delete product in the cart
$(document).on('click', '.cart_item .product-remove a', function (e)
{
    e.preventDefault();

    var product_id = $(this).attr("data-product_id"),
        cart_item_key = $(this).attr("data-cart_item_key"),
        product_container = $(this).parents('.cart_item');
    // Add loader
    // product_container.block({
    //     message: null,
    //     overlayCSS: {
    //         cursor: 'none'
    //     }
    // });
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url:wc_add_to_cart_params.ajax_url,
        data: {
            action: "product_remove",
            product_id: product_id,
            cart_item_key: cart_item_key
        },
        success: function(response) {
            if ( ! response || response.error )
                return;

            var fragments = response.fragments;

            // Replace fragments
            if ( fragments ) {
                $.each( fragments, function( key, value ) {
                    $( key ).replaceWith( value );
                });
            }
            window.location.reload();
        }
    });
});