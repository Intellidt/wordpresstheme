<?php
/**
 * Customer on-hold order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-on-hold-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined('ABSPATH') || exit;

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
//do_action( 'woocommerce_email_header', $email_heading, $email );
// ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>"/>
    <title><?php echo get_bloginfo('name', 'display'); ?></title>
</head>
<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="
0" style="font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'; ?>">
  <div style="padding: 30px;margin: 0 auto;width: 60%;background: white;font-size:14px;">
      <div style="width:50%;float:left">
          <img src="<?php echo get_template_directory_uri(); ?>/images/cpn_logo.png" height="120"/>
      </div>
      <div style="width: 50%; float: left;height: 90px; line-height: 90px;text-align: right;">Order #<?php echo $order->get_order_number(); ?></div>
      <div style="clear:both;"></div>
      <div style="color:#000;margin-top:15px;font-size:16px;"><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></div>
      <div style="margin-top:5px;">
          <?php
            if ( $partial_refund ) {
            	/* translators: %s: Site title */
            	printf( esc_html__( 'Your order on %s has been partially refunded. There are more details below for your reference:', 'woocommerce' ), wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES ) ); // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped
            } else {
            	/* translators: %s: Site title */
            	printf( esc_html__( 'Your order on %s has been refunded. There are more details below for your reference:', 'woocommerce' ), wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES ) ); // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped
            }
        ?>
      </div>
      <hr/>
      <div style="margin-top:30px">
          <p><strong>Order summary</strong></p>
          <?php
            do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );
          ?>
      </div>
      
      <hr/>
      <div style="margin-top:30px">
          <p style="margin-bottom:20px;"><strong>Customer information</strong></p>
          <?php 
          do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );
          ?>
      </div>
      <hr/>
      <div style="margin:30px 0px">
         <p><strong>Please note: </strong>This email message was sent from a notification only address and cannot accept incoming email. Please do not reply to this message. For any questions regarding this order, please contact us by email at xxxxxxxx@xxxxxxxxxxxxx or by phone at xxx-xxx-xxxx.</p>
        <p>Thanks again for shopping with us.</p>
 
      </div>    
  </div>
  
</div>
</body>
</html>