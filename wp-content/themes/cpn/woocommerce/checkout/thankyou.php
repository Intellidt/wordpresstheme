<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="container">
    <div class="woocommerce-order mt-5">

	<?php if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() ); ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you for your order, ', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?><strong><?php echo "#".$order->get_order_number(); ?></strong></p>
		
		    <p class="mb-3"><strong>Purchasing Information:</strong></p>

		    <?php //do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		
        
            <?php $show_shipping = ! wc_ship_to_billing_address_only() && $order->needs_shipping_address();
?>
            <section class="woocommerce-customer-details">
            
            	<?php if ( $show_shipping ) : ?>
            
            	<section class="woocommerce-columns woocommerce-columns--2 woocommerce-columns--addresses col2-set addresses">
            		<div class="woocommerce-column woocommerce-column--1 woocommerce-column--billing-address col-6 float-left">
            
            	<?php endif; ?>
            
            	    <h6 class="woocommerce-column__title"><strong><?php esc_html_e( 'Billing address :', 'woocommerce' ); ?></strong></h6>
            
                    	<address>
                    		<?php echo wp_kses_post( $order->get_formatted_billing_address( esc_html__( 'N/A', 'woocommerce' ) ) ); ?>
                    
                    		<?php if ( $order->get_billing_phone() ) : ?>
                    			<p class="woocommerce-customer-details--phone"><?php echo esc_html( $order->get_billing_phone() ); ?></p>
                    		<?php endif; ?>
                    
                    		<?php if ( $order->get_billing_email() ) : ?>
                    			<p class="woocommerce-customer-details--email"><?php echo esc_html( $order->get_billing_email() ); ?></p>
                    		<?php endif; ?>
                    	</address>
            
            	    <?php if ( $show_shipping ) : ?>
            
            		</div><!-- /.col-1 -->
            
            		<div class="woocommerce-column woocommerce-column--2 woocommerce-column--shipping-address col-6 float-left">
            			<h6 class="woocommerce-column__title"><strong><?php esc_html_e( 'Shipping address :', 'woocommerce' ); ?></strong></h6>
            			<address>
            				<?php echo wp_kses_post( $order->get_formatted_shipping_address( esc_html__( 'N/A', 'woocommerce' ) ) ); ?>
            			</address>
            		</div><!-- /.col-2 -->
                    <div class="clearfix"></div>
            	</section><!-- /.col2-set -->
            
            	<?php endif; ?>
            
            </section>
            
            <?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
            
        <?php endif; ?>
        
	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you for your order.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>

</div>
</div>
