<style>
    .woocommerce div.product div.images img {
        display: block;
        height: auto;
        box-shadow: none;
        width: 210px;
    }
</style>

<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$tabs = apply_filters( 'woocommerce_product_tabs', array() );


?>
<?php if (class_exists('WooCommerce') && is_woocommerce()) : ?>
   
<?php endif; ?>
<div class="container py-5">
  <?php if (class_exists('WooCommerce') && is_woocommerce()) : ?>
       <?php woocommerce_breadcrumb(); ?>
  <?php endif; ?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

	<?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">
		<?php
		/**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */
		do_action( 'woocommerce_single_product_summary' );
		?>
	

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>
	</div>
	<div class="clearfix"></div>  
	<?php if ( ! empty( $tabs ) ) : ?>

<!--
        <?php foreach ($tabs as $key => $tab) : ?>
            <?php if($key != "reviews" && $key!="benefits") { ?>
                <div class="card">
                    <div class="card-header" id="product-<?php echo esc_attr($key); ?>">
                        <h5 class="mb-0">
                            <a class="btn btn-link w-100 text-left collapsed" data-toggle="collapse"
                               data-target="#product-<?php echo esc_attr($key); ?>-area" aria-expanded="true"
                               aria-controls="product-<?php echo esc_attr($key); ?>-area"><?php echo apply_filters('woocommerce_product_' . $key . '_tab_title', esc_html($tab['title']), $key); ?></a>

                        </h5>
                    </div>
                    <div id="product-<?php echo esc_attr($key); ?>-area" class="collapse"
                         aria-labelledby="product-<?php echo esc_attr($key); ?>" data-parent="#product-description">
                        <div class="card-body" style="padding-left:35px">
                            <?php if (isset($tab['callback'])) {
                                call_user_func($tab['callback'], $key, $tab);
                            } ?>
                        </div>
                    </div>
                </div>
            <?php }  ?>
        <?php endforeach; ?>
    -->





        <div class="product-reviews">
            <?php if (isset($tabs['reviews']) && isset($tabs['reviews']['callback'])) {
                    call_user_func($tabs['reviews']['callback'], "reviews", $tabs['reviews']);
            } ?>
        </div>
    <?php endif; ?>
</div>
</div>
<div class="wp-block-group background-light-footer"><div class="wp-block-group__inner-container">
        <div class="wp-block-columns">
            <div class="wp-block-column">
                <div class="wp-block-columns">
                    <div class="wp-block-column footer-highlight">
                        <div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://cpn.intelligrp.com/wp-content/uploads/2020/03/icon_canada_made.png" alt="" class="wp-image-2489"></figure></div>



                        <h4 class="text-center">Canadian Made</h4>



                        <h5 class="color-black text-center">CPN supplement are 100% formulated, made and packaged in Canada, according to Health Canada standards</h5>
                    </div>



                    <div class="wp-block-column footer-highlight">
                        <div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://cpn.intelligrp.com/wp-content/uploads/2020/03/icon_gmo.png" alt="" class="wp-image-2494"></figure></div>



                        <h4 class="text-center">GMP Standard</h4>



                        <h5 class="color-black text-center">All products are manufactured in a controlled and consistently monitored environment to ensure each product meets their quality standard</h5>
                    </div>
                </div>
            </div>



            <div class="wp-block-column">
                <div class="wp-block-columns">
                    <div class="wp-block-column footer-highlight">
                        <div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://cpn.intelligrp.com/wp-content/uploads/2020/03/icon_health_canada-1.png" alt="" class="wp-image-2492"></figure></div>



                        <h4 class="text-center">Health Canada</h4>



                        <h5 class="color-black text-center">CPN products have been approved by Health Canada as natural health products, ensuring all products are safe, effective and of high quality</h5>
                    </div>



                    <div class="wp-block-column footer-highlight">
                        <div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://cpn.intelligrp.com/wp-content/uploads/2020/03/icon_fda-1.png" alt="" class="wp-image-2490"></figure></div>



                        <h4 class="text-center">FDA Compliance</h4>



                        <h5 class="color-black text-center">Our manufacturing facility meets the GMP standards and has been approved by the US FDA as an acceptable manufacturing facility</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php do_action( 'woocommerce_after_single_product' ); ?>

