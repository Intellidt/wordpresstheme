<!DOCTYPE html >
<html>
    <head>
        <?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>
<div class="page-wraper">

<div class="menu-list-container position-relative">
    <div class="float-left py-3">
        <a class="d-block" href="http://cpnhealth.ca/">
                <img class="header_logo" src="<?php echo get_template_directory_uri(); ?>/images/logo_top.png" class="d-block m-auto" alt="<?php bloginfo('name'); ?>" />
        </a>
    </div>
    <div class="float-right menu_list">
         <div class="d-inline-block mr-3 menu_bar_container">
            <?php wp_nav_menu(
                    array(
                            'theme_location' => 'top-menu',
                            'menu_class' => 'navigation',
    
                        )
            )?>
        </div>
        
    </div>
    <div class="clear"></div>
</div>

