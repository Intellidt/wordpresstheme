<?php
/**
 *yogic-lite About Theme
 *
 * @package Yogic Lite
 */

//about theme info
add_action( 'admin_menu', 'yogic_lite_abouttheme' );
function yogic_lite_abouttheme() {    	
	add_theme_page( __('About Theme Info', 'yogic-lite'), __('About Theme Info', 'yogic-lite'), 'edit_theme_options', 'yogic_lite_guide', 'yogic_lite_mostrar_guide');   
} 

//Info of the theme
function yogic_lite_mostrar_guide() { 	
?>
<div class="wrap-GT">
	<div class="gt-left">
   		   <div class="heading-gt">
			  <h3><?php esc_html_e('About Theme Info', 'yogic-lite'); ?></h3>
		   </div>
          <p><?php esc_html_e('Yogic Lite is a free meditation WordPress theme specially designed to create professional and elegant website for yoga, meditation, pilates, wellness center, personal coach and health related websites. This multipurpose theme can also be used for charity, NGO, church, gym, sports club, beauty and spa websites.', 'yogic-lite'); ?></p>
<div class="heading-gt"> <?php esc_html_e('Theme Features', 'yogic-lite'); ?></div>
 

<div class="col-2">
  <h4><?php esc_html_e('Theme Customizer', 'yogic-lite'); ?></h4>
  <div class="description"><?php esc_html_e('The built-in customizer panel quickly change aspects of the design and display changes live before saving them.', 'yogic-lite'); ?></div>
</div>

<div class="col-2">
  <h4><?php esc_html_e('Responsive Ready', 'yogic-lite'); ?></h4>
  <div class="description"><?php esc_html_e('The themes layout will automatically adjust and fit on any screen resolution and looks great on any device. Fully optimized for iPhone and iPad.', 'yogic-lite'); ?></div>
</div>

<div class="col-2">
<h4><?php esc_html_e('Cross Browser Compatible', 'yogic-lite'); ?></h4>
<div class="description"><?php esc_html_e('Our themes are tested in all mordern web browsers and compatible with the latest version including Chrome,Firefox, Safari, Opera, IE11 and above.', 'yogic-lite'); ?></div>
</div>

<div class="col-2">
<h4><?php esc_html_e('E-commerce', 'yogic-lite'); ?></h4>
<div class="description"><?php esc_html_e('Fully compatible with WooCommerce plugin. Just install the plugin and turn your site into a full featured online shop and start selling products.', 'yogic-lite'); ?></div>
</div>
<hr />  
</div><!-- .gt-left -->
	
<div class="gt-right">    
     <a href="http://www.gracethemesdemo.com/yogic/" target="_blank"><?php esc_html_e('Live Demo', 'yogic-lite'); ?></a> | 
    <a href="http://www.gracethemesdemo.com/documentation/yogic/#homepage-lite" target="_blank"><?php esc_html_e('Documentation', 'yogic-lite'); ?></a>    
</div><!-- .gt-right-->
<div class="clear"></div>
</div><!-- .wrap-GT -->
<?php } ?>