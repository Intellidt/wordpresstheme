<?php
/**
 * Trigger this file on plugin uninstall
 * 
 * @package ProductPlugin
 */

 if( ! defined('WP_UNINSTALL_PLUGIN')){
     die;
 }
 //clear database store data
 //$products = get_posts(array('post_type' => 'product','numberposts' => -1));

foreach( $products as $product) {
      wp_delete_post( $product->ID, true);
}

// access the database via SQL
global $wpdb;
$wpdb->query("DELETE FROM wp_post WHERE post_type");
$wpdb->query("DELETE FROM wp_postmeta WHERE post_id NOT IN  (SELECT id FROM wp_posts)");
$wpdb->query("DELETE FROM wp_term_relationship WHERE object_id NOT IN  (SELECT id FROM wp_posts)");