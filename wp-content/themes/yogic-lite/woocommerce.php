<?php get_header(); ?>
<div class="container">
<div id="yogic_lite_content_wrapper">
     <div class="content_section_right fullwidth">  
			<?php woocommerce_content(); ?>
    </div><!-- content_section_right-->   
</div><!-- #yogic_lite_content_wrapper --> 
</div><!-- .container -->     
<?php get_footer(); ?>