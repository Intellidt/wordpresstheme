<?php

//* Add custom sections and setting to the Admin customizer
class TheMinimalist_Customizer{
     
    public function __construct(){
        add_action('customizer_register',array($this,'register_customize_section'));
    }

    public function register_customize_sections ($wp_customize){
        // initialize section
        $this->slider_callout_section($wp_customize);  
    }

    private function slider_callout_section($wp_customize){
        // new panel;
        $wp_customize->add_section('basic-slider-callout-section',array(
        'title'=>'slider',
        'priority' => 2,
        'description' => __('The slider section is displayed on home page.','theminimallist')
    ));

    // add setting
    $wp_customize -> add_setting('basic-slider-callout-display',array(
    'default' => 'no',
    'sanitize_callback' => array($this,'sanitize_custom_option')
    ));

    // add control
    $wp_customizer->add_control(new WP_customize_control($wp_customize,'basic-slider-callout-control',array(
        'label' => 'Display this section',
        'section' => 'basic-slider-callout-section',
        'settings' => 'basic-slider-callout-display',
        'type' => 'select',
        'choices' => array ('no','yes' => 'yes')
    )));

    // Add Slider Image
    $wp_customize->add_setting('basic-slider-callout-image',array(
        'default' => '',
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => array($this,'sanitize_custom_url')
    ));
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'basic-slider-callout-image-control',array(
        'label' => 'image',
        'section' => 'basic-author-callout-section',
        'settings' => 'basic-author-callout-image',
        'width' => 442,
        'height' => 310

    )));

    }

    // public function sanitize_custom_option($input){
    //     return($input === "no") ? "no":"yes";
    // }
    
    // public function sanitize_custom_option($input){
    //     return filter_var($input, FILTER_SANITIZE_STRING);
    // }

    // public function sanitize_custom_option($input){
    //     return filter_var($input, FILTER_SANITIZE_STRING);
    // }

}