<?php
/**
 * @package ProductPlugin
 */
/*
Plugin Name: Product Plugin
Plugin URI : http://ProductPlugin.com/Plugin
Description: This is my first attempt for creating a custom plugin.
Version : 1.0.0
Author : Tanishq Methwani
Author URi : http://TanishqMethwani.com
License : GPLv2 or later
Text Domain : Product-Plugin
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of GNU General Public License 
as published by theFree Software Foundation: either version 2 
of the license, or any later version

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software 
Foundation,Inc., 51 franklin street, Fifth floor,Boston, MA 02110-1301, USA

Copyright 2005-2015 Automatic , Inc.
*/


defined('ABSPATH') or die('hey,you cant access this file');

class ProductPlugin
{
 
    function __construct(){
        add_action('init',array($this,'custom_post_type'));
    }

    function activate(){
        // generated a CPT
        // flush rewrite rules
   }

   function deactivate(){
        // flush rewrite the rules
   }
   function uninstall(){
        // delete CPT
        // delete all plugin data from the DB
   }
   function custom_post_type(){
       register_post_type('Appearance',['public'=>true,'label'=> 'Appearance']);
   }
      
}

if( class_exists('ProductPlugin')){
$productPlugin = new ProductPlugin();
}

// activation
register_activation_hook(__FILE__,array($ProductPlugin,'activate'));

// deactivation
register_deactivation_hook(__FILE__,array($ProductPlugin,'deactivate'));

// uninstall