</div>
<!-- #main .wrapper -->
</div>
<!---- page wrapper --->
<footer id="footer" class="menu-list-container">

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-7">
                <p class="text-left mt-3 footer-text"><?php create_copyright(); ?></p>
                <p class="text-left mt-3 footer-small-text">
                Claims made about specific products on or through this website have not been evaluated by Health Canada.  Products are not approved to diagnose, treat, cure or prevent disease. The information provided on this site is for informational purposes only and is not intended as a substitute for individual medical advice from your physician or other health care professional.
                </p>

            </div>
            <div class="col-sm-5">
                <?php if ( is_active_sidebar( 'footer-sidebar' ) ) : ?>
                    <div class="footer-widget-area" >
                        <div class="footer">
                            <div class="text-left">
                                <?php dynamic_sidebar( 'footer-sidebar' ); ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    </footer>
</div>
<!-- #page -->
<?php wp_footer(); ?>
</body>
</html>