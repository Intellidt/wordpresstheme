<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Yogic Lite
 */

get_header(); ?>

<div class="container">
    <div id="yogic_lite_content_wrapper">
        <div class="content_section_right">
            <header class="page-header">
                <h1 class="entry-title"><?php esc_html_e( '404 Not Found', 'yogic-lite' ); ?></h1>                
            </header><!-- .page-header -->
            <div class="page-content">
                <p><?php esc_html_e( 'Looks like you have taken a wrong turn.....Dont worry... it happens to the best of us.', 'yogic-lite' ); ?></p>  
            </div><!-- .page-content -->
        </div><!-- content_section_right-->   
        <?php get_sidebar();?>       
        <div class="clear"></div>
    </div>
</div>
<?php get_footer(); ?>