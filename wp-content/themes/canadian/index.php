<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Canadian Nutritech</table>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="style.css">
</head>
<body>
<nav>
<div id="slideout-menu">
<ul>
	  <li>
	  <a href="#">Home</a>
	  </li>
	  <li>
	  <a href="#">Shop</a>
	  </li>
	  <li>
	  <a href="#">FAQ</a>
	  </li>
	  <li>
	  <a href="#">About</a>
	  </li>
	  <li>
	  <input type="text" placeholder="search here">
	  <br>
	  <div id="search-icon">
	    <i class="fas fa-search"></i>
		</div>
	  </li>
	</ul>
</div>
</nav>
<div id="searchbox">
  <input type="text" placeholder="Search Here">
</div>
<div id="banner">
<h1>&lt;Canadian Nutritech/&gt;</h1>
</div>
<main>
  <a href="#">
    <h2 class="section-heading">All blogs</h2>
  </a>
  <section>
    <div class="card">
	  <div class="card-image">
	    <a href="#">
		  <h3>The Blog title</h3>
		</a>
		<a href="#" class="btn-readmore">Read more</a>
		</div>
		</div>

		<div class="card">
	  <div class="card-image">
	    <a href="#">
		  <h3>The Blog title</h3>
		</a>
		<a href="#" class="btn-readmore">Read more</a>
		</div>
		</div>
  </section>

<h2 class="section-heading">All Projects</h2>

  <section>
    <div class="card">
	  <div class="card-image">
	    <a href="#">
		  <h3>Project title</h3>
		</a>
		<a href="#" class="btn-readmore">Read more</a>
		</div>
		</div>

		<div class="card">
	  <div class="card-image">
	    <a href="#">
		  <h3>Project title</h3>
		  
		</a>
		<a href="#" class="btn-readmore">Read more</a>
		</div>
		</div>
  </section>
</main>
</body>
</html>