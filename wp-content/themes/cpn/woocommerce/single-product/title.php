<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @package    WooCommerce/Templates
 * @version    1.6.4
 */
global $product;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

the_title( '<div class="mb-4"><div class="float-left w-50"><h2 class="product_title entry-title text-uppercase">', '</h2></div><div class="float-right">'.(get_post_meta($product->id, '_upcoming', true ) ? "<div class='coming_soon_label'>Coming Soon</div>" : "").'</div><div class="clear"></div></div>' );
?>

