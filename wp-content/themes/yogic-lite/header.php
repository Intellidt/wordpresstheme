<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="container">
 *
 * @package Yogic Lite
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php endif; ?>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php
	if ( function_exists( 'wp_body_open' ) ) {
		wp_body_open();
	} else {
		do_action( 'wp_body_open' );
	}
?>
<a class="skip-link screen-reader-text" href="#yogic_lite_content_wrapper">
<?php esc_html_e( 'Skip to content', 'yogic-lite' ); ?>
</a>
<?php
$yogic_lite_show_contactinfo_strip 	               = esc_attr( get_theme_mod('yogic_lite_show_contactinfo_strip', false) );
$yogic_lite_show_hdr_right_social_part        	   = esc_attr( get_theme_mod('yogic_lite_show_hdr_right_social_part', false) ); 
$yogic_lite_show_front_page_slider 	               = esc_attr( get_theme_mod('yogic_lite_show_front_page_slider', false) );
$yogic_lite_show_three_column_services_area 	   = esc_attr( get_theme_mod('yogic_lite_show_three_column_services_area', false) );
?>
<div id="sitelayout" <?php if( get_theme_mod( 'yogic_lite_site_layout_options' ) ) { echo 'class="boxlayout"'; } ?>>
<?php
if ( is_front_page() && !is_home() ) {
	if( !empty($yogic_lite_show_front_page_slider) ) {
	 	$inner_cls = '';
	}
	else {
		$inner_cls = 'siteinner';
	}
}
else {
$inner_cls = 'siteinner';
}

if( !empty($yogic_lite_show_contactinfo_strip) || ($yogic_lite_show_hdr_right_social_part) ) {
	 	$inner_logo = '';
	} else {
		$inner_logo = 'innerlogo';
	}

?>


<div class="site-header <?php echo esc_attr($inner_cls); ?> "> 
  
  <div class="yogic_lite_topbar">
       <div class="container">
            <div class="left">	
             <?php if( $yogic_lite_show_contactinfo_strip != ''){ ?>		
              <?php $yogic_lite_cellphoneno = get_theme_mod('yogic_lite_cellphoneno');
               if( !empty($yogic_lite_cellphoneno) ){ ?>              
                 <div class="yogic_lite_infobx">
                     <i class="fas fa-phone-volume"></i>               
                     <span><?php echo esc_html($yogic_lite_cellphoneno); ?></span>   
                 </div>       
             <?php } ?> 
             
             
              <?php 
            $email = get_theme_mod('yogic_lite_headeremail_info');
               if( !empty($email) ){ ?>                
                 <div class="yogic_lite_infobx">
                     <i class="fas fa-envelope-open-text"></i>
                     <span>
                        <a href="<?php echo esc_url('mailto:'.sanitize_email($email)); ?>"><?php echo sanitize_email($email); ?></a>
                    </span> 
                </div>            
              <?php } ?>   
            <?php } ?>   
            </div>
            
            <div class="right">            
            <?php if( $yogic_lite_show_hdr_right_social_part != ''){ ?>
              
                    <div class="yogic_lite_hdrsocial">                                                
					   <?php $yogic_lite_facebook_link = get_theme_mod('yogic_lite_facebook_link');
                        if( !empty($yogic_lite_facebook_link) ){ ?>
                        <a title="<?php esc_html_e('Facebook','yogic-lite'); ?>" class="fab fa-facebook-f" target="_blank" href="<?php echo esc_url($yogic_lite_facebook_link); ?>"></a>
                       <?php } ?>
                    
                       <?php $yogic_lite_twitter_link = get_theme_mod('yogic_lite_twitter_link');
                        if( !empty($yogic_lite_twitter_link) ){ ?>
                        <a title="<?php esc_html_e('Twitter','yogic-lite'); ?>" class="fab fa-twitter" target="_blank" href="<?php echo esc_url($yogic_lite_twitter_link); ?>"></a>
                       <?php } ?>
                
                      <?php $yogic_lite_googleplus_link = get_theme_mod('yogic_lite_googleplus_link');
                        if( !empty($yogic_lite_googleplus_link) ){ ?>
                        <a title="<?php esc_html_e('Google Plus','yogic-lite'); ?>" class="fab fa-google-plus" target="_blank" href="<?php echo esc_url($yogic_lite_googleplus_link); ?>"></a>
                      <?php }?>
                
                      <?php $yogic_lite_linkedin_link = get_theme_mod('yogic_lite_linkedin_link');
                        if( !empty($yogic_lite_linkedin_link) ){ ?>
                        <a title="<?php esc_html_e('Linkedin','yogic-lite'); ?>" class="fab fa-linkedin" target="_blank" href="<?php echo esc_url($yogic_lite_linkedin_link); ?>"></a>
                      <?php } ?> 
                      
                      <?php $yogic_lite_instagram_link = get_theme_mod('yogic_lite_instagram_link');
                        if( !empty($yogic_lite_instagram_link) ){ ?>
                        <a title="<?php esc_html_e('Instagram','yogic-lite'); ?>" class="fab fa-instagram" target="_blank" href="<?php echo esc_url($yogic_lite_instagram_link); ?>"></a>
                      <?php } ?> 
                                       
                   </div><!--end .yogic_lite_hdrsocial-->                 
             <?php } ?>   
            
            </div>
            <div class="clear"></div>
        </div><!-- .container-->
  </div><!-- .yogic_lite_topbar-->
  
  <div class="container">      
      <div class="logo <?php echo esc_attr($inner_logo); ?>">
           <?php yogic_lite_the_custom_logo(); ?>
            <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
            <?php $description = get_bloginfo( 'description', 'display' );
            if ( $description || is_customize_preview() ) : ?>
                <p><?php echo esc_html($description); ?></p>
            <?php endif; ?>
      </div><!-- logo -->    
  </div><!-- .container --> 
  
  
  
   <div class="yogic_lite_hdrmenu"> 
     <div class="container">    
       
       <div id="topnavigator" class="<?php echo esc_attr($inner_logo); ?>" role="banner">
		<button class="menu-toggle" aria-controls="main-navigation" aria-expanded="false" type="button">
			<span aria-hidden="true"><?php esc_html_e( 'Menu', 'yogic-lite' ); ?></span>
			<span class="dashicons" aria-hidden="true"></span>
		</button>

		<nav id="main-navigation" class="site-navigation primary-navigation" role="navigation">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'primary',
				'container' => 'ul',
				'menu_id' => 'primary',
				'menu_class' => 'primary-menu menu',
			) );
			?>
		</nav><!-- #site-navigation -->
	</div><!-- #topnavigator -->
       
       
       
      <div class="clear"></div> 
  </div><!-- .container-->
  
</div><!-- .yogic_lite_hdrmenu-->
  

  
</div><!--.site-header --> 
 
<?php 
if ( is_front_page() && !is_home() ) {
if($yogic_lite_show_front_page_slider != '') {
	for($i=1; $i<=3; $i++) {
	  if( get_theme_mod('yogic_lite_frontpage_slider'.$i,false)) {
		$slider_Arr[] = absint( get_theme_mod('yogic_lite_frontpage_slider'.$i,true));
	  }
	}
?> 
<div class="frontpage_slider">                
<?php if(!empty($slider_Arr)){ ?>
<div id="slider" class="nivoSlider">
<?php 
$i=1;
$slidequery = new WP_Query( array( 'post_type' => 'page', 'post__in' => $slider_Arr, 'orderby' => 'post__in' ) );
while( $slidequery->have_posts() ) : $slidequery->the_post();
$image = wp_get_attachment_url( get_post_thumbnail_id($post->ID)); 
$thumbnail_id = get_post_thumbnail_id( $post->ID );
$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); 
?>
<?php if(!empty($image)){ ?>
<img src="<?php echo esc_url( $image ); ?>" title="#slidecaption<?php echo esc_attr( $i ); ?>" alt="<?php echo esc_attr($alt); ?>" />
<?php }else{ ?>
<img src="<?php echo esc_url( get_template_directory_uri() ) ; ?>/images/slides/slider-default.jpg" title="#slidecaption<?php echo esc_attr( $i ); ?>" alt="<?php echo esc_attr($alt); ?>" />
<?php } ?>
<?php $i++; endwhile; ?>
</div>   

<?php 
$j=1;
$slidequery->rewind_posts();
while( $slidequery->have_posts() ) : $slidequery->the_post(); ?>                 
    <div id="slidecaption<?php echo esc_attr( $j ); ?>" class="nivo-html-caption">         
    	<h2><?php the_title(); ?></h2>
    	<?php the_excerpt(); ?>
		<?php
        $yogic_lite_frontpage_slider_btntext = get_theme_mod('yogic_lite_frontpage_slider_btntext');
        if( !empty($yogic_lite_frontpage_slider_btntext) ){ ?>
            <a class="slide_morebtn" href="<?php the_permalink(); ?>"><?php echo esc_html($yogic_lite_frontpage_slider_btntext); ?></a>
        <?php } ?>                  
    </div>   
<?php $j++; 
endwhile;
wp_reset_postdata(); ?>   
<?php } ?>
 </div><!-- .frontpage_slider -->    
<?php } } ?>

   
        
<?php if ( is_front_page() && ! is_home() ) { ?>
<?php if( $yogic_lite_show_three_column_services_area != ''){ ?>   
<div id="yogic_lite_services_section">
  <div class="container">	                 
               <?php 
                for($n=1; $n<=3; $n++) {    
                if( get_theme_mod('yogic_lite_three_column_page'.$n,false)) {      
                    $queryvar = new WP_Query('page_id='.absint(get_theme_mod('yogic_lite_three_column_page'.$n,true)) );		
                    while( $queryvar->have_posts() ) : $queryvar->the_post(); ?>     
                    <div class="yogic_lite_3column <?php if($n % 3 == 0) { echo "last_column"; } ?>">                                       
                        <?php if(has_post_thumbnail() ) { ?>
                        <div class="services_image_box"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a></div>        
                        <?php } ?>
                        <div class="pagedesc_box">              	
                          <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>  
                          <?php the_excerpt();?>
                          <a class="learnmore" href="<?php the_permalink(); ?>"><?php esc_html_e('Read More','yogic-lite'); ?></a>
                        </div>                      
                    </div>
                    <?php endwhile;
                    wp_reset_postdata();                                  
                } } ?>                                 
            <div class="clear"></div>        
      <div class="clear"></div>
    </div><!-- .container -->
</div><!-- #yogic_lite_services_section -->
<?php } ?>
<?php } ?>