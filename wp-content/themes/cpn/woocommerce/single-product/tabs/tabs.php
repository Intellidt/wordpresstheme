<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>
      
    <div id="product">
        <?php foreach ($tabs as $key => $tab) : ?>
             <?php
            if($key != "reviews") {
                if ($key == "benefits") { ?>
                    <div class="product-info-container mt-5" id="product-<?php echo esc_attr($key); ?>">
                        <h5 class="produc-info-head">
                            <?= apply_filters('woocommerce_product_' . $key . '_tab_title', esc_html($tab['title']), $key); ?>
                        </h5>
                        <div class="product-info-body">
                            <?php if (isset($tab['callback'])) {
                                call_user_func($tab['callback'], $key, $tab);
                            } ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="card">
                        <div class="card-header" id="product-<?php echo esc_attr($key); ?>">
                            <h5 class="mb-0">
                                <a class="btn btn-link w-100 text-left collapsed" data-toggle="collapse"
                                   data-target="#product-<?php echo esc_attr($key); ?>-area" aria-expanded="true"
                                   aria-controls="product-<?php echo esc_attr($key); ?>-area"><?php echo apply_filters('woocommerce_product_' . $key . '_tab_title', esc_html($tab['title']), $key); ?></a>

                            </h5>
                        </div>
                        <div id="product-<?php echo esc_attr($key); ?>-area" class="collapse"
                             aria-labelledby="product-<?php echo esc_attr($key); ?>" data-parent="#product-description">
                            <div class="card-body" style="padding-left:35px">
                                <?php if (isset($tab['callback'])) {
                                    call_user_func($tab['callback'], $key, $tab);
                                } ?>
                            </div>
                        </div>
                    </div>
                <?php }
            }?>

        <?php endforeach; ?>
    </div>





   
<?php endif; ?>
