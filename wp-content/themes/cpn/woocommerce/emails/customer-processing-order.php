<?php
/**
 * Customer on-hold order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-on-hold-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined('ABSPATH') || exit;
 
               
/*
 * @hooked WC_Emails::email_header() Output the email header
 */
//do_action( 'woocommerce_email_header', $email_heading, $email );
// ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>"/>
    <title><?php echo get_bloginfo('name', 'display'); ?></title>
</head>
<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="
0" style="font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'; ?>">
  <div style="padding: 30px;margin: 0 auto;width: 60%;background: white;font-size:14px;">
      <div style="width:50%;float:left">
          <img src="<?php echo get_template_directory_uri(); ?>/images/cpn_logo.png" height="120"/>
      </div>
      <div style="width: 50%; float: left;height: 90px; line-height: 90px;text-align: right;">Order #<?php echo $order->get_order_number(); ?></div>
      <div style="clear:both;"></div>
      <div style="color:#000;margin-top:15px;font-size:18px;">Your order is on the way</div>
      <div style="margin-top:5px;">Your order is on the way. Track your shipment to see the delivery status.</div>
       <div style="margin-top:30px">
         <button class="button" onclick="window.location='http://cpn.intelligrp.com/'">Visit our store</button>
       </div>
       <div style="margin:30px 0px">
           <?php
                $order_id = $order->get_order_number();
                $tracking_number = $order->get_meta('ywot_tracking_code');
           ?>
          <p><span style="color:#767676">Tracking number : </span><?php echo $tracking_number; ?></p>
       </div>
       <hr/>
      <div style="margin:30px 0px">
          <p><strong>Items in this shipment</strong></p>
          <div style="margin: 20px 0px;">
            	<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="0">
            		<thead>
            			<tr>
            				<th class="td" scope="col" style="text-align:left;width:55%"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
            				<th class="td" scope="col" style="text-align:left;"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></th>
            				<th class="td" scope="col" style="text-align:left;"><?php esc_html_e( 'Price', 'woocommerce' ); ?></th>
            			</tr>
            		</thead>
            		<tbody>
            			<?php
            			echo wc_get_email_order_items( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            				$order,
            				array(
            					'show_sku'      => $sent_to_admin,
            					'show_image'    => false,
            					'image_size'    => array( 32, 32 ),
            					'plain_text'    => $plain_text,
            					'sent_to_admin' => $sent_to_admin,
            				)
            			);
            			?>
            		</tbody>
            	</table>
          </div>
      </div>
      <div style="margin:30px 0px">
         <p><strong>Please note: </strong>This email message was sent from a notification only address and cannot accept incoming email. Please do not reply to this message. For any questions regarding this order, please contact us by email at xxxxxxxx@xxxxxxxxxxxxx or by phone at xxx-xxx-xxxx.</p>
        <p>Thanks again for shopping with us.</p>
 
      </div>    
  </div>
  
</div>
</body>
</html>
