<?php    
/**
 *yogic-lite Theme Customizer
 *
 * @package Yogic Lite
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function yogic_lite_customize_register( $wp_customize ) {	
	
	function yogic_lite_sanitize_dropdown_pages( $page_id, $setting ) {
	  // Ensure $input is an absolute integer.
	  $page_id = absint( $page_id );
	
	  // If $page_id is an ID of a published page, return it; otherwise, return the default.
	  return ( 'publish' == get_post_status( $page_id ) ? $page_id : $setting->default );
	}

	function yogic_lite_sanitize_checkbox( $checked ) {
		// Boolean check.
		return ( ( isset( $checked ) && true == $checked ) ? true : false );
	}  
		
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	
	 //Panel for section & control
	$wp_customize->add_panel( 'yogic_lite_panel_section', array(
		'priority' => null,
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => __( 'Theme Options Panel', 'yogic-lite' ),		
	) );
	
	//Site Layout Options
	$wp_customize->add_section('yogic_lite_layout_option',array(
		'title' => __('Site Layout Options','yogic-lite'),			
		'priority' => 1,
		'panel' => 	'yogic_lite_panel_section',          
	));		
	
	$wp_customize->add_setting('yogic_lite_site_layout_options',array(
		'sanitize_callback' => 'yogic_lite_sanitize_checkbox',
	));	 

	$wp_customize->add_control( 'yogic_lite_site_layout_options', array(
    	'section'   => 'yogic_lite_layout_option',    	 
		'label' => __('Check to Show Box Layout','yogic-lite'),
		'description' => __('If you want to show box layout please check the Box Layout Option.','yogic-lite'),
    	'type'      => 'checkbox'
     )); //Site Layout Options 
	
	$wp_customize->add_setting('yogic_lite_site_color_codes',array(
		'default' => '#cf317c',
		'sanitize_callback' => 'sanitize_hex_color'
	));
	
	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,'yogic_lite_site_color_codes',array(
			'label' => __('Color Options','yogic-lite'),			
			'description' => __('More color options available in PRO Version','yogic-lite'),
			'section' => 'colors',
			'settings' => 'yogic_lite_site_color_codes'
		))
	);		
	
	//Top Contact Info
	$wp_customize->add_section('yogic_lite_contactdetails_info',array(
		'title' => __('Header Contact Details','yogic-lite'),				
		'priority' => null,
		'panel' => 	'yogic_lite_panel_section',
	));	
	
	$wp_customize->add_setting('yogic_lite_cellphoneno',array(
		'default' => null,
		'sanitize_callback' => 'sanitize_text_field'	
	));
	
	$wp_customize->add_control('yogic_lite_cellphoneno',array(	
		'type' => 'text',
		'label' => __('Enter cell number here','yogic-lite'),
		'section' => 'yogic_lite_contactdetails_info',
		'setting' => 'yogic_lite_cellphoneno'
	));	
	
	
	$wp_customize->add_setting('yogic_lite_headeremail_info',array(
		'sanitize_callback' => 'sanitize_email'
	));
	
	$wp_customize->add_control('yogic_lite_headeremail_info',array(
		'type' => 'email',
		'label' => __('enter email id here.','yogic-lite'),
		'section' => 'yogic_lite_contactdetails_info'
	));	
	
	
	$wp_customize->add_setting('yogic_lite_show_contactinfo_strip',array(
		'default' => false,
		'sanitize_callback' => 'yogic_lite_sanitize_checkbox',
		'capability' => 'edit_theme_options',
	));	 
	
	$wp_customize->add_control( 'yogic_lite_show_contactinfo_strip', array(
	   'settings' => 'yogic_lite_show_contactinfo_strip',
	   'section'   => 'yogic_lite_contactdetails_info',
	   'label'     => __('Check To show This Section','yogic-lite'),
	   'type'      => 'checkbox'
	 ));//Show Contact Details Strip
	 
	 
	 //Header Right Social icons part
	$wp_customize->add_section('yogic_lite_hdr_right_social_part',array(
		'title' => __('Header Right Social icons','yogic-lite'),
		'description' => __( 'Add social icons link here to display icons in header right', 'yogic-lite' ),			
		'priority' => null,
		'panel' => 	'yogic_lite_panel_section', 
	));
	
	$wp_customize->add_setting('yogic_lite_facebook_link',array(
		'default' => null,
		'sanitize_callback' => 'esc_url_raw'	
	));
	
	$wp_customize->add_control('yogic_lite_facebook_link',array(
		'label' => __('Add facebook link here','yogic-lite'),
		'section' => 'yogic_lite_hdr_right_social_part',
		'setting' => 'yogic_lite_facebook_link'
	));	
	
	$wp_customize->add_setting('yogic_lite_twitter_link',array(
		'default' => null,
		'sanitize_callback' => 'esc_url_raw'
	));
	
	$wp_customize->add_control('yogic_lite_twitter_link',array(
		'label' => __('Add twitter link here','yogic-lite'),
		'section' => 'yogic_lite_hdr_right_social_part',
		'setting' => 'yogic_lite_twitter_link'
	));
	
	$wp_customize->add_setting('yogic_lite_googleplus_link',array(
		'default' => null,
		'sanitize_callback' => 'esc_url_raw'
	));
	
	$wp_customize->add_control('yogic_lite_googleplus_link',array(
		'label' => __('Add google plus link here','yogic-lite'),
		'section' => 'yogic_lite_hdr_right_social_part',
		'setting' => 'yogic_lite_googleplus_link'
	));
	
	$wp_customize->add_setting('yogic_lite_linkedin_link',array(
		'default' => null,
		'sanitize_callback' => 'esc_url_raw'
	));
	
	$wp_customize->add_control('yogic_lite_linkedin_link',array(
		'label' => __('Add linkedin link here','yogic-lite'),
		'section' => 'yogic_lite_hdr_right_social_part',
		'setting' => 'yogic_lite_linkedin_link'
	));
	
	$wp_customize->add_setting('yogic_lite_instagram_link',array(
		'default' => null,
		'sanitize_callback' => 'esc_url_raw'
	));
	
	$wp_customize->add_control('yogic_lite_instagram_link',array(
		'label' => __('Add instagram link here','yogic-lite'),
		'section' => 'yogic_lite_hdr_right_social_part',
		'setting' => 'yogic_lite_instagram_link'
	));
	
	$wp_customize->add_setting('yogic_lite_show_hdr_right_social_part',array(
		'default' => false,
		'sanitize_callback' => 'yogic_lite_sanitize_checkbox',
		'capability' => 'edit_theme_options',
	));	 
	
	$wp_customize->add_control( 'yogic_lite_show_hdr_right_social_part', array(
	   'settings' => 'yogic_lite_show_hdr_right_social_part',
	   'section'   => 'yogic_lite_hdr_right_social_part',
	   'label'     => __('Check To show This Section','yogic-lite'),
	   'type'      => 'checkbox'
	 ));//Show Header Right Social icons part
	
	
	// Header Slider Section		
	$wp_customize->add_section( 'yogic_lite_front_page_slider_panel', array(
		'title' => __('Front Page Slider', 'yogic-lite'),
		'priority' => null,
		'description' => __('Default image size for slider is 1400 x 693 pixel.','yogic-lite'), 
		'panel' => 	'yogic_lite_panel_section',           			
    ));
	
	$wp_customize->add_setting('yogic_lite_frontpage_slider1',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'yogic_lite_sanitize_dropdown_pages'
	));
	
	$wp_customize->add_control('yogic_lite_frontpage_slider1',array(
		'type' => 'dropdown-pages',
		'label' => __('Select page for slider 1:','yogic-lite'),
		'section' => 'yogic_lite_front_page_slider_panel'
	));	
	
	$wp_customize->add_setting('yogic_lite_frontpage_slider2',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'yogic_lite_sanitize_dropdown_pages'
	));
	
	$wp_customize->add_control('yogic_lite_frontpage_slider2',array(
		'type' => 'dropdown-pages',
		'label' => __('Select page for slider 2:','yogic-lite'),
		'section' => 'yogic_lite_front_page_slider_panel'
	));	
	
	$wp_customize->add_setting('yogic_lite_frontpage_slider3',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'yogic_lite_sanitize_dropdown_pages'
	));
	
	$wp_customize->add_control('yogic_lite_frontpage_slider3',array(
		'type' => 'dropdown-pages',
		'label' => __('Select page for slider 3:','yogic-lite'),
		'section' => 'yogic_lite_front_page_slider_panel'
	));	// Front page Slider Section
	
	$wp_customize->add_setting('yogic_lite_frontpage_slider_btntext',array(
		'default' => null,
		'sanitize_callback' => 'sanitize_text_field'	
	));
	
	$wp_customize->add_control('yogic_lite_frontpage_slider_btntext',array(	
		'type' => 'text',
		'label' => __('enter slider Read more button name here','yogic-lite'),
		'section' => 'yogic_lite_front_page_slider_panel',
		'setting' => 'yogic_lite_frontpage_slider_btntext'
	)); // Slider Read More Button name
	
	$wp_customize->add_setting('yogic_lite_show_front_page_slider',array(
		'default' => false,
		'sanitize_callback' => 'yogic_lite_sanitize_checkbox',
		'capability' => 'edit_theme_options',
	));	 
	
	$wp_customize->add_control( 'yogic_lite_show_front_page_slider', array(
	    'settings' => 'yogic_lite_show_front_page_slider',
	    'section'   => 'yogic_lite_front_page_slider_panel',
	     'label'     => __('Check To Show This Section','yogic-lite'),
	   'type'      => 'checkbox'
	 ));//Show Front Page Slider Panle	
	 
	 
	 //Three Column Services Sections
	$wp_customize->add_section('yogic_lite_three_column_services_area', array(
		'title' => __('Three Column Services Section','yogic-lite'),
		'description' => __('Select pages from the dropdown for services section','yogic-lite'),
		'priority' => null,
		'panel' => 	'yogic_lite_panel_section',          
	));	
	
	$wp_customize->add_setting('yogic_lite_three_column_page1',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'yogic_lite_sanitize_dropdown_pages'
	));
 
	$wp_customize->add_control(	'yogic_lite_three_column_page1',array(
		'type' => 'dropdown-pages',			
		'section' => 'yogic_lite_three_column_services_area',
	));		
	
	$wp_customize->add_setting('yogic_lite_three_column_page2',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'yogic_lite_sanitize_dropdown_pages'
	));
 
	$wp_customize->add_control(	'yogic_lite_three_column_page2',array(
		'type' => 'dropdown-pages',			
		'section' => 'yogic_lite_three_column_services_area',
	));
	
	$wp_customize->add_setting('yogic_lite_three_column_page3',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'yogic_lite_sanitize_dropdown_pages'
	));
 
	$wp_customize->add_control(	'yogic_lite_three_column_page3',array(
		'type' => 'dropdown-pages',			
		'section' => 'yogic_lite_three_column_services_area',
	));	
	
	$wp_customize->add_setting('yogic_lite_show_three_column_services_area',array(
		'default' => false,
		'sanitize_callback' => 'yogic_lite_sanitize_checkbox',
		'capability' => 'edit_theme_options',
	));	 
	
	$wp_customize->add_control( 'yogic_lite_show_three_column_services_area', array(
	   'settings' => 'yogic_lite_show_three_column_services_area',
	   'section'   => 'yogic_lite_three_column_services_area',
	   'label'     => __('Check To Show This Section','yogic-lite'),
	   'type'      => 'checkbox'
	 ));//Show Three Column Services Section
	 
	 
	//Sidebar Settings
	$wp_customize->add_section('yogic_lite_sidebar_settings', array(
		'title' => __('Sidebar Settings','yogic-lite'),		
		'priority' => null,
		'panel' => 	'yogic_lite_panel_section',          
	));	
	
	$wp_customize->add_setting('yogic_lite_hidesidebar_homepage_latest_post',array(
		'default' => false,
		'sanitize_callback' => 'yogic_lite_sanitize_checkbox',
		'capability' => 'edit_theme_options',
	));	 
	
	$wp_customize->add_control( 'yogic_lite_hidesidebar_homepage_latest_post', array(
	   'settings' => 'yogic_lite_hidesidebar_homepage_latest_post',
	   'section'   => 'yogic_lite_sidebar_settings',
	   'label'     => __('Check to hide sidebar from homepage latest post','yogic-lite'),
	   'type'      => 'checkbox'
	 )); //Hide sidebar from homepage latest post
	 
	 
	 $wp_customize->add_setting('yogic_lite_hidesidebar_singlepost',array(
		'default' => false,
		'sanitize_callback' => 'yogic_lite_sanitize_checkbox',
		'capability' => 'edit_theme_options',
	));	 
	
	$wp_customize->add_control( 'yogic_lite_hidesidebar_singlepost', array(
	   'settings' => 'yogic_lite_hidesidebar_singlepost',
	   'section'   => 'yogic_lite_sidebar_settings',
	   'label'     => __('Check to hide sidebar from single post','yogic-lite'),
	   'type'      => 'checkbox'
	 )); //hide sidebar single post	 

	 
		 
}
add_action( 'customize_register', 'yogic_lite_customize_register' );

function yogic_lite_custom_css(){ 
?>
	<style type="text/css"> 					
        a, .default_article_list h2 a:hover,
        #sidebar ul li a:hover,						
        .default_article_list h3 a:hover, 
		.site-navigation .menu a:hover,
		.site-navigation .menu a:focus,
		.site-navigation ul li a:hover, 
		.site-navigation ul li.current_page_item a, 
		.site-navigation ul li.current_page_item ul li a:hover,
		.site-navigation ul li.current-menu-parent a, 
		.site-navigation ul li:hover,
		.site-navigation .menu ul a:hover,
		.site-navigation .menu ul a:focus,
		.site-navigation ul li.current_page_item ul.sub-menu li a:hover, 
		.site-navigation ul li.current-menu-parent ul.sub-menu li a:hover,
		.site-navigation ul li.current-menu-parent ul.sub-menu li ul.sub-menu li a:hover,
		.site-navigation ul li.current-menu-parent ul.sub-menu li.current_page_item a,    						
        .postmeta a:hover,
		.yogic_lite_hdrsocial a:hover,			
        .button:hover,
		.yogic_lite_3column:hover h3 a,	
		.blog_postmeta a:hover,	
		.site-footer ul li a:hover, 
		.site-footer ul li.current_page_item a		
            { color:<?php echo esc_html( get_theme_mod('yogic_lite_site_color_codes','#cf317c')); ?>;}					 
            
        .pagination ul li .current, .pagination ul li a:hover, 
        #commentform input#submit:hover,		
        .nivo-controlNav a.active,		
		a.blogreadmore,	
		.nivo-caption .slide_morebtn,
		.logo,
		h3.widget-title,		
		.learnmore:hover,
		.yogic_lite_3column .services_image_box,
		.yogic_lite_3column:hover .learnmore,											
        #sidebar .search-form input.search-submit,				
        .wpcf7 input[type='submit'],				
        nav.pagination .page-numbers.current,		
		.blogpostmorebtn:hover,		
        .toggle a	
            { background-color:<?php echo esc_html( get_theme_mod('yogic_lite_site_color_codes','#cf317c')); ?>;}
			
		
		.tagcloud a:hover,		
		.yogic_lite_hdrsocial a:hover,		
		h3.widget-title::after	
            { border-color:<?php echo esc_html( get_theme_mod('yogic_lite_site_color_codes','#cf317c')); ?>;}						
	
			
         	
    </style> 
<?php                                                            
}
         
add_action('wp_head','yogic_lite_custom_css');	 

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function yogic_lite_customize_preview_js() {
	wp_enqueue_script( 'yogic_lite_customizer', get_template_directory_uri() . '/js/customize-preview.js', array( 'customize-preview' ), '28042020', true );
}
add_action( 'customize_preview_init', 'yogic_lite_customize_preview_js' );