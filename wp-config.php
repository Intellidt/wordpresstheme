<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'intelli website' );

/** MySQL database username */
define( 'DB_USER', 'intelli' );

/** MySQL database password */
define( 'DB_PASSWORD', '123abcttt' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'LNV%[B/<qJ <mZFkc{-9wDP5]NK>j 1znP6e=BpaHcMCq>B!,YN8VbBo%-mK --C' );
define( 'SECURE_AUTH_KEY',  '^`o8y&53 i.fqAIp.d-UaW1A=MeYc8BjTV]pRK?^0xCljVgH rc!^Ta$k>&RX&8>' );
define( 'LOGGED_IN_KEY',    'M+lrG_%[g%_`nU5_}NaZ+qE40+qj2W6.)BrBj,V~C <vXRejD#,$)~V-%_8&3H9L' );
define( 'NONCE_KEY',        '?0tv0Fs;^R_J1EcZ`Dwe>V uBqST3DqzGmME9<WyAQHmui3>!/vN*-+U-;=VQ<6;' );
define( 'AUTH_SALT',        'PgqQD8nsJ9nR5;4HzD:pZuH4sojC#Edk`,8nL(nGdFr-E_7[d_2G(;sE5K+*e1z=' );
define( 'SECURE_AUTH_SALT', '$NPkF5 P+~ZabdcFF%sp-/H}VtC]%crFR~C-{0V@pTAOv uE~5I&wLX98P~$06Z=' );
define( 'LOGGED_IN_SALT',   '9>3t BXVsO^Qz]r#6<fwc:pSyhqr{ lWlSU+{*8V?1@y|k053#uX~YhjKtY:p=ZP' );
define( 'NONCE_SALT',       'Fc3rC9#!9[aX7]DQ)t~0HBJoL9=TU{P_JUA u:8m>/;s2|.WCb-&1k2dl-MMDxC`' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
