=== Yogic Lite ===
Contributors: gracethemes
Tags: blog, two-columns, right-sidebar, full-width-template, custom-colors, custom-menu, custom-header, 
custom-logo, featured-images, editor-style, custom-background, threaded-comments, theme-options,
translation-ready
Requires at least: 5.0
Requires PHP:  5.2
Tested up to: 5.4.1
License: GNU General Public License version 2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Yogic Lite is a free meditation WordPress theme specially designed to create professional and elegant website for yoga, meditation, pilates, wellness center, personal coach and health related websites. This multipurpose theme can also be used for charity, NGO, church, gym, sports club, beauty and spa websites.

== Theme License & Copyright ==
* Yogic Lite WordPress Theme, Copyright 2020 Grace Themes
* Yogic Lite is distributed under the terms of the GNU GPL

== Changelog ==

= 1.0 =
* Intial version release

= 1.1 =
* added keyboard navigation functions
*readme file issue and validated
* fixed  mobie view keyboard navigation
* fixed transientsissue from  template-tags.php file
* added wp body open proper condition in header.php file
* proper sanitize email code

= 1.2 =
* escaped theme mods functions in customizer.php
* added Tested up to & Requires PHP version in style.css file New required header fields
* removed stable tag from readme.txt file and google font license

= 1.3 =
* added proper wp_reset_postdata() in template-tags.php file


== Resources ==

Theme is Built using the following resource bundles.

= jQuery Nivo Slider =
* Name of Author: Dev7studios
* Copyright: 2010-2012
* https://github.com/Codeinwp/Nivo-Slider-jQuery
* License: MIT License
* https://github.com/Codeinwp/Nivo-Slider-jQuery/blob/master/license.txt

= Customizer-Pro =
* Name of Author: Justin Tadlock
* Copyright 2016, Justin Tadlock justintadlock.com
* https://github.com/justintadlock/trt-customizer-pro
* License: GNU General Public License v2.0
* http://www.gnu.org/licenses/gpl-2.0.html


= Images =
Image for theme screenshot, Copyright pxhere.com
License: CC0 1.0 Universal (CC0 1.0)
Source: https://pxhere.com/en/photo/700368 (slider image)


= Images =
Image for theme screenshot, Copyright pxhere.com
License: CC0 1.0 Universal (CC0 1.0)
Source: https://pxhere.com/en/photo/1419835 (services image1)


= Images =
Image for theme screenshot, Copyright pxhere.com
License: CC0 1.0 Universal (CC0 1.0)
Source: https://pxhere.com/en/photo/826575 (services image2)


= Images =
Image for theme screenshot, Copyright pxhere.com
License: CC0 1.0 Universal (CC0 1.0)
Source: https://pxhere.com/en/photo/1419151 (services image3)



= Font Awesome =
* https://fontawesome.com/icons

# Icons: CC BY 4.0 License (https://creativecommons.org/licenses/by/4.0/)
	In the Font Awesome Free download, the CC BY 4.0 license applies to all icons packaged as SVG and JS file types.

# Fonts: SIL OFL 1.1 License (https://scripts.sil.org/OFL)
	In the Font Awesome Free download, the SIL OFL license applies to all icons packaged as web and desktop font files.

# Code: MIT License (https://opensource.org/licenses/MIT)
	In the Font Awesome Free download, the MIT license applies to all non-font and non-icon files.

= Dashicons =
* License: dashicons is licensed under GPLv2
* https://github.com/WordPress/dashicons

   
For any help you can mail us at support@gracethemes.com
